# Mkdocs

## Install

```bash
$ sudo pip install mkdocs
```

Verify installation

```bash
$ mkdocs --version
mkdocs, version 1.1.2 from /usr/local/lib/python3.9/site-packages/mkdocs (Python 3.9)
```

## Themes

My prefer theme is Mkdocs Material

```bash
pip install mkdocs-material
```

# Getting Started

For create a mkdocs project is so simple

```bash
$ mkdocs new mkdocs-project
$ cd mkdocs-project
```

For set Mkdocs Material Theme add the following line in `mkdocs.yml`

```yml
theme:
  name: material
```

And that's it, for running in live

```bash
$ mkdocs serve
```

## Resources

https://www.mkdocs.org

https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes

https://github.com/squidfunk/mkdocs-material