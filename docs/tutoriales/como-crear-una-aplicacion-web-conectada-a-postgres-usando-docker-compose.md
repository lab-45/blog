# Cómo ejecutar una aplicación Spring Boot con Postgres usando Docker Compose

## Prerrequisitos

- Java 11
- Maven

## Paso 1: Creamos nuestro proyecto

Lo primero que vamos a hacer es crear nuestro proyecto. Para ello Spring nos ofrece la herramienta [Spring Initializr](https://start.spring.io/) que nos va a permitir definirle desde el nombre al proyecto hasta selecionar las dependencias.

Para este caso particular vamos a necesitar **PostgresSQL Driver** y **Spring Data JPA**

![Screenshot](img/0000001.png)

Cuando estemos listos solo debemos descargar nuestro proyecto haciendo click en el botón **Generate**.

> Nota: La herramienta **Spring Initializr** no es de uso obligatorio pero si estás empezando o querés ahorrarte tiempo valioso puede serte de gran ayuda.

## Paso 2: Probar el proyecto

Una vez que hemos descargado nuestro proyecto lo descomprimimos.

![Screenshot](img/0000002.png)

Por línea de comandos nos dirigimos a la carpeta donde quedó descomprimido nuestro proyecto y procedemos a ajecutar lo siguiente

```bash
mvn spring-boot:run
```

![Screenshot](img/0000003.png)

Como aún no tenemos la base de datos lo que nos debe mostrar al final de la descarga de dependencias es el siguiente error:

![Screenshot](img/0000004.png)

## Paso 3: Crear Dockerfile

Empaquetar la aplicación y saltarse los test

```bash
mvn clean package -DskipTests
```

Creamos la carpeta docker y copiamos el empaquetado a esta ruta

```bash
mkdir docker
cp target/web-app-db-docker-0.0.1-SNAPSHOT.jar docker/
```

Este debe ser el resultado esperado:

![Screenshot](img/0000005.png)

Y por último creamos el `Dockerfile` en la ruta `docker` con el siguiente contenido

```bash
FROM openjdk:11
ARG JAR_FILE=*.jar
COPY ${JAR_FILE} application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]
```

Así debe quedar la estructura de la carpeta

```bash
ll docker
```

![Screenshot](img/0000006.png)


## Paso 4: Crear Docker Compose

El archivo Docker Compose es el que nos va a permitir orquestar nuestra aplicación con la imagen de la base de datos.

Lo vamos a crear junto a nuestro archivo de `Dockerfile` que definimos en la carpeta `docker`.

```bash
touch docker/docker-compose.yml
```

Ahora lo editamos y le agregamos el siguiente contenido:


> Importante: It turns out you cannot have a table named user in postgres. So simply changing the table name from user to user_entity fixed this.


## Recursos
[Running Spring Boot with PostgreSQL in Docker Compose](https://www.baeldung.com/spring-boot-postgresql-docker)
[Spring Initializr](https://start.spring.io/)

https://stackoverflow.com/questions/37980463/postgres-not-creating-tables-properly-with-hibernate/37980703