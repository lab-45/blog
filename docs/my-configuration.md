# My Configuration

This is my configuration applied on my computer. Is based on Fedora 34 Spin KDE


## Speed up dnf

```bash
sudo nano /etc/dnf/dnf.conf

max_parallel_downloads=10
fastestmirror=True
```

## Update OS

After a fresh installation of Fedora, the first step is update with lasted version

```bash
sudo dnf update
sudo dnf upgrade
reboot
```


## Remove Unused Package

I like to remove these packages because I don't need then, for manage email I use directly from browser and in the case of Kwrite, I prefer Kate (I will install it later).

```bash
sudo dnf remove kontact kmail kwrite
```

## Enable RPM Fusion

Basically RPM Fussion allow me get it software from third party repositories.

```bash
sudo dnf install \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y

sudo dnf install \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
```

## Enable Flatpak

Flatpak is another initiative like Snap (supported by Canonical) that allow me to install or develop an aplication with all necessary dependencies (like a sandbox) independently of the distro.

```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

## Install 1Password

```bash
sudo rpm --import https://downloads.1password.com/linux/keys/1password.asc
sudo sh -c 'echo -e "[1password]\nname=1Password Stable Channel\nbaseurl=https://downloads.1password.com/linux/rpm/stable/\$basearch\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=\"https://downloads.1password.com/linux/keys/1password.asc\"" > /etc/yum.repos.d/1password.repo'
sudo dnf install 1password
```

## Instal NordVPN

```bash
sh <(curl -sSf https://downloads.nordcdn.com/apps/linux/install.sh)
sudo usermod -aG nordvpn $USER
newgrp nordvpn
nordvpn login
nordvpn connect
```

## Install OpenVPN

```bash
sudo dnf install openvpn -y
```


## Download the openVpn configuration files

```bash
cd
mkdir .nordvpn_ovpn_conf
cd .nordvpn_ovpn_conf
wget https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip
unzip ovpn.zip
```

For validate server recommended using this page [Server recommended by NordVPN](https://nordvpn.com/servers/tools/?_ga=2.99507862.96768235.1656083759-1322967621.1656083758)


## Install Development Tools

The Development Tools Group is great selection of packages for developers and I really appreciate the Fedora simplicity give me, without complexity to install package after package... and again... install package after package. :bomb:

```bash
sudo dnf groupinstall "Development Tools" -y
```

## Install Multimedia Codecs

To get better experience using video streaming services I installed these software:

```bash
sudo dnf install gstreamer1-plugins-{bad-*,good-*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel -y

sudo dnf install lame* --exclude=lame-devel -y
sudo dnf group upgrade --with-optional Multimedia -y
```

## Install Kate

Kate is a powerfull text editor provided for KDE community.

```bash
sudo dnf install kate
```

## Install Ansible

Ansible is an amazing tool to automate: applications, infrastructure, networking, etc, etc.

```bash
sudo dnf install ansible -y
```

## Install Docker

The first step is register repos and install docker

```bash
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo

sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
```

Next one is add the current user to docker group

```bash
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
```

And finally start docker and run a test

```bash
sudo systemctl start docker
docker run hello-world
```

> Optional: to start docker on boot run following commands

```bash
systemctl enable docker.service
systemctl enable containerd.service
```

## Install Docker Compose

Docker Compose is a tool for manage multi-container applications, and you can configure writing in YML format

```bash
sudo dnf install docker-compose -y
```

## Install Postman

Postman provide many features, from simple request to orchestrate complex execution.

```bash
flatpak install flathub com.getpostman.Postman -y
```

## Install Dbeaver

DBeaver is nice multiplatform client for connect to database.

```bash
flatpak install flathub io.dbeaver.DBeaverCommunity -y
```

## Install Spotify

Well, just music... :guitar:

```bash
flatpak install flathub com.spotify.Client -y
```

## Install Insync

And this is the story: a few years ago I needed an application allow me sync in MacOS, Windows and Linux with same configuration: uncomplicated. So I decided to buy Insync because it covered my needs. All the time I have been using it, it has always met my expectations: in Mac OS, Windows Elementary OS, Ubuntu, KDE Neon, POP! OS and now Fedora.

```bash
sudo rpm --import https://d2t3ff60b2tol4.cloudfront.net/repomd.xml.key

cat <<\EOF | sudo tee /etc/yum.repos.d/insync.repo > /dev/null
[insync]
name=insync repo
baseurl=http://yum.insync.io/fedora/$releasever/
gpgcheck=1
gpgkey=https://d2t3ff60b2tol4.cloudfront.net/repomd.xml.key
enabled=1
metadata_expire=120m
EOF

sudo dnf install insync
```

## Install Visual Code

Visual Code give me: **simplicity**, good performance, a lot of plugins and it's for dummies

```bash
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
sudo dnf check-update -y
sudo dnf install code -y
```

## Tunning the Command Line

The command line is a great place to live, it is a "home", so what better than to decorate your home with your preferences?

**Install ZSH (and other necessary packages) and OhMyZSH:**

```bash
sudo dnf install git wget curl ruby ruby-devel zsh util-linux-user redhat-rpm-config gcc gcc-c++ make
sudo dnf install fontawesome-fonts -y
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

To set default SHELL to ZSH execute this:
```bash
sudo chsh -s /bin/zsh
```

**Install Powerlevel10k:**

Powerlevel10k is a theme for zsh

```bash
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

Edit `~/.zshrc` and set `ZSH_THEME="powerlevel10k/powerlevel10k"`. Reload zshrc for configure:

```bash
source ~/.zshrc
```

**Install Color LS:**

Allow you beautify ls output adding icons and colors

```bash
sudo gem install colorls
```

Edit file: `~/.zshrc` and add these lines to final file

```bash
alias ll='colorls -lA --sd --gs --group-directories-first'
alias ls='colorls --group-directories-first'
```

```bash
source $(dirname $(gem which colorls))/tab_complete.sh
```

> To reconfigure powerlevel10k execute: `p10k configure`


>***Important:***
>
>I couldn't change default shell with `chsh -s /bin/zsh`, so I had to edit profile in Konsole (Settings -> Edit Current Profile)
>
>Command: /bin/zsh

## Install Latte Dock

Latte Dock is a... dock, that's it. 
But Latte Dock has a lot of features, like a change size, background, margin, align, blur, effects, style, etc, etc, etc.

```bash
sudo dnf install latte-dock
latte-dock
```


https://archived.forum.manjaro.org/t/howto-use-modifieronlyshortcuts-on-kde/67650

I needed mapping Super Key to open aplication launcher from Latte Dock, and I found two options:

This is my prefer option:

```bash
kwriteconfig5 --file ~/.config/kwinrc --group ModifierOnlyShortcuts --key Meta "org.kde.lattedock,/Latte,org.kde.LatteDock,activateLauncherMenu"
qdbus org.kde.KWin /KWin reconfigure
```

Or edit `~/.config/kwinrc` and add:

```bash
[ModifierOnlyShortcuts]
Meta=org.kde.lattedock,/Latte,org.kde.LatteDock,activateLauncherMenu
```

If latte-dock not load on login, is important to verify if application is **added** in `System Settings > Startup and Shutdown`


## ...And last one

I had many Fedora entries when I started my computer and I had only one OS installed, so for hide all entries and start automatically with lasted entry, I did that:

```bash
sudo su
grub2-editenv - set menu_auto_hide=1
```
## Resources

[Enabling the RPM Fusion repositories](https://docs.fedoraproject.org/en-US/quick-docs/setup_rpmfusion/)

[Installing Spotify on Fedora](https://docs.fedoraproject.org/en-US/quick-docs/installing-spotify/)

[Visual Studio Code on Linux](https://code.visualstudio.com/docs/setup/linux)

[Oh My ZSH!](https://ohmyz.sh/)
[Install and Setup ZSH and Oh-My-Zsh on Fedora 32](https://kifarunix.com/install-and-setup-zsh-and-oh-my-zsh-on-fedora-32/)

[Powerlevel10k](https://github.com/romkatv/powerlevel10k)

[Color LS](https://github.com/athityakumar/colorls)

[Tuning your bash or zsh shell on Fedora Workstation and Silverblue](https://fedoramagazine.org/tuning-your-bash-or-zsh-shell-in-workstation-and-silverblue/)

[Change my default shell in Linux using chsh](https://www.cyberciti.biz/faq/change-my-default-shell-in-linux-using-chsh/)

[Latte Dock](https://github.com/KDE/latte-dock)

[LatteDock/FAQ](https://userbase.kde.org/LatteDock/FAQ)

[insynchq](https://www.insynchq.com/)
